# terraform-pipeline

## Pupose

Provides a GitLab CI for terraform with the configuration of both `terraform backend` and the cloud `provider` externalised as GitLab CI/CD variables called respectively `$TFCD_BACKEND` and `$TFCD_PROVIDER`. This two variables can be set at different group level to factorize the Terraform configuration and let you define it once and otherwise focus on and keep only the terraform code of your infrastructure only in your git repositories.  

Before executing terraform command line, the pipeline writes the content of `$TFCD_BACKEND` into `backend.tf` and `$TFCD_PROVIDER` into `provider.tf`.  

This project [terraform-exemple](https://gitlab.com/Astrakhan.fr/cicd/terraform-exemple) is a simple example, that creates a gcs bucket.

## Usage

- Add a `.gitlab-ci.yml` with the following content

  ```yaml
  include:
  - project: Astrakhan.fr/cicd/terraform-pipeline
    ref: master
    file: terraform.gitlab-ci.yml
  ```

- Configure the GitLab CI/CD variables below
  - `GITLAB_TOKEN` a GitLab personal access token to push teraform state to GitLab
  - `TFCD_BACKEND` can be set once at group level for multiple projects for all cloud providers

    ```json
    terraform {
        required_providers {
            random = "~> 2.2"
        }
        backend "http" {
            address        = "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/gcp"
            lock_address   = "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/gcp/lock"
            unlock_address = "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/gcp/lock"
            username       = "tf"
            lock_method    = "POST"
            unlock_method  = "DELETE"
            retry_wait_min = "5"
        }
    }
    ```

  - GCP Provider
    - `TFCD_PROVIDER` can be set once at group level for multiple projects

    ```json
    provider "google" {
        project = var.gcp_project
        region  = var.gcp_region
        zone    = var.gcp_zone
    }
    variable "gcp_project" {}
    variable "gcp_region" {}
    variable "gcp_zone" {}
    variable "gitlab_project_id" {}
    variable "application" {}
    ```

    - `GOOGLE_CLOUD_KEYFILE_JSON`: a JSON key file for the GCP service account used by terraform to manage resources
    - `TF_VAR_gcp_project`: gcp project's id
    - `TF_VAR_gcp_region`: gcp region where create resources (ex. us-central1)
    - `TF_VAR_gcp_zone`: gcp zpne where create resources (ex. us-central1-c)
